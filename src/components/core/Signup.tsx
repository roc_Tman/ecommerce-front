import Layout from './Layout'
import  { Form, Input, Button } from 'antd'

const Signup = () => {
  return (
    <div>
      <Layout title="注册" subTitle="">
        <Form>
          <Form.Item name="name" label="昵称">
            <Input/>
          </Form.Item>
          <Form.Item name="password" label="密码">
            <Input.Password/>
          </Form.Item>
          <Form.Item name="email" label="邮箱">
            <Input/>
          </Form.Item>
          <Form.Item name="email">
            <Button type="primary" htmlType="submit">注册</Button>
          </Form.Item>
        </Form>
      </Layout>
    </div>
  )
}

export default Signup
