import { useSelector } from 'react-redux'
import Layout from './Layout'

const Home = () => {
  const state = useSelector(state => state)
  return <Layout title="首页" subTitle="home subTitle">Home {JSON.stringify(state)}</Layout>
}

export default Home
